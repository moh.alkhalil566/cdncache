# Build the image with the name "kafka-spark"
docker build -t kafka-spark .

# Start a container with the name "kafka-spark" from the image "kafka-spark"
# Bind the current directory in the source to the path "/kafka-spark/from_source" inside the container
# After this step the zookeeper and the kafka servers are running
docker run --name kafka-spark -it -v ${PWD}:/kafka-spark/from_source kafka-spark

# Execute the kafka_producer.py inside the container in a new terminal window
docker exec -it kafka-spark python /kafka-spark/from_source/src/kafka_producer.py

# Execute the spark_consumer.py inside the container in a new terminal window
docker exec -it kafka-spark spark-submit --master local --driver-memory 4g --num-executors 2 --executor-memory 4g --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.1 /kafka-spark/from_source/src/spark_consumer.py

# The output csv files are written to ./out folder
# If a single csv file is desired, execute the merge_csv.py inside the container to merge the csv files
docker exec -it kafka-spark python /kafka-spark/from_source/src/merge_csv.py


docker exec -it openface FeatureExtraction.exe -f $DATA_MOUNT/videos/_dI--eQ6qVU_1.mp4 -out_dir $DATA_MOUNT
