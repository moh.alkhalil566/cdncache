from kafka import KafkaProducer
import pandas as pd


TOPIC_NAME = 'requests'
DATA_FILE = '/kafka-spark/from_source/cdndataset.csv'

df = pd.read_csv(DATA_FILE)
df['time_received'] = pd.to_datetime(df['time_received'])
df = df.drop(index=len(df) - 1)
df = df.sort_values('time_received')

producer = KafkaProducer(bootstrap_servers='localhost:9092',
                         value_serializer=lambda v: v.encode('utf-8'),
                         )
for index, row in df.astype(str).iterrows():
    row = ','.join(row.tolist())
    print(row)
    producer.send(topic=TOPIC_NAME, value=row)
producer.flush()
