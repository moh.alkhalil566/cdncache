import os
import glob
import pandas as pd

os.chdir('/kafka-spark/from_source/out')
all_filenames = [i for i in glob.glob('*.csv')]
merged_csv = pd.concat([pd.read_csv(f) for f in all_filenames])
merged_csv.to_csv('/kafka-spark/from_source/out/merged.csv', index=False, encoding='utf-8-sig')
