from kafka import KafkaConsumer
# from time import sleep


TOPIC_NAME = 'requests'

consumer = KafkaConsumer(
    bootstrap_servers=['localhost:9092'],
    auto_offset_reset='latest',
    enable_auto_commit=True,
    group_id='my-group-id',
    value_deserializer=lambda v: v.decode('utf-8'),
)
consumer.subscribe(topics=[TOPIC_NAME])

for event in consumer:
    event_data = event.value
    print(event_data)
    # sleep(1)
