from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as F


TOPIC_NAME = 'requests'

spark = SparkSession.builder \
    .appName("Spark Structured Streaming from Kafka") \
    .getOrCreate()
spark.sql("set spark.sql.legacy.timeParserPolicy=LEGACY")

requests_sdf = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "localhost:9092") \
    .option("subscribe", TOPIC_NAME) \
    .option("startingOffsets", "latest") \
    .option("includeTimestamp", True) \
    .load()
requests_sdf = requests_sdf.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

schema = StructType([
    StructField("Unnamed", LongType()),
    StructField("time_received", TimestampType()),
    StructField("byte_sent", FloatType()),
    StructField("host_id", LongType()),
    StructField("time_to_serve", FloatType()),
    StructField("response_status", StringType()),
    StructField("content_type", LongType()),
    StructField("request_method", StringType()),
    StructField("user_id", FloatType()),
    StructField("session_id", LongType()),
    StructField("live_tv_id", FloatType()),
    StructField("vod_id", FloatType()),
    StructField("vod_encoding", FloatType()),
    StructField("user_ip", LongType()),
    StructField("request_header", FloatType()),
])


def parse_data_from_kafka_message(sdf, schema):
    from pyspark.sql.functions import split
    assert sdf.isStreaming, "DataFrame doesn't receive streaming data"
    col = split(sdf['value'], ',')  # split attributes to nested array in one Column
    # now expand col to multiple top-level columns
    for idx, field in enumerate(schema):
        if field.name == 'time_received':
            sdf = sdf.withColumn(field.name, F.to_timestamp(col.getItem(idx), 'yyyy-MM-dd HH:mm:ss'))
        else:
            sdf = sdf.withColumn(field.name, col.getItem(idx).cast(field.dataType))
    return sdf.select([field.name for field in schema])


requests_sdf = parse_data_from_kafka_message(requests_sdf, schema)
requests_sdf = requests_sdf.withColumn("tcp_hit", (F.trim(requests_sdf.response_status) == "tcp_hit").cast("int"))

df = requests_sdf


'''df.withColumn("repr", F.col("response_status").cast("binary"))\
    .withColumn("hit", F.trim(df.response_status) == "tcp_hit")\
    .select(["response_status", "repr", "hit"]) \
    .writeStream.format("console").option("truncate", "False").start()'''

'''df = df.withColumn("hit2", F.when(df.response_status == "tcp_hit", "Male")
                            .when(df.response_status == "tcp_miss", "Female")
                            .when(df.response_status.isNull(), "")
                            .otherwise(repr(df.response_status)))'''

'''# Group the data by window and word and compute the count of each group
windowDuration = "2 seconds"
slideDuration = "1 seconds"
df = df.withWatermark("time_received", "3 seconds")\
    .groupBy(F.window(df.time_received, windowDuration, slideDuration))\
    .mean().orderBy('window').select("avg(tcp_hit)")'''

df.writeStream \
    .outputMode("append") \
    .format("csv") \
    .option("header", True) \
    .option("path", "/kafka-spark/from_source/out") \
    .option("checkpointLocation", "/kafka-spark/from_source/out/checkpoint") \
    .option("truncate", False) \
    .trigger(processingTime="10 seconds") \
    .start() \
    .awaitTermination()
