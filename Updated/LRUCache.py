from typing import Dict, Optional, List

class Node:
    """
    Linked List Node. Contains key-value pair and links to neighbor elements.
    """

    def __init__(self, key: int, value: int, prev=None, next=None):
        self.key: int = key
        self.value: int = value

        self.prev: Optional[Node] = prev
        self.next: Optional[Node] = next


class LinkedList:
    """
    Linked List. Represents usage history of cache items
    """

    head: Optional[Node] = None
    tail: Optional[Node] = None

    def add_to_head(self, item: Node) -> None:
        """
        Add node to the very top of the list
        """

        if self.head is not None:
            item.next = self.head
            self.head.prev = item

        if self.tail is None:
            self.tail = item

        self.head = item

    def unlink(self, item: Node) -> None:
        """
        Remove references to the node from other nodes on the list
        """
        if item is None:
            return

        prev_item: Node = item.prev
        next_item: Node = item.next

        # unlink the item node:
        # link prev and next items
        # removing referenced to the current item node
        if prev_item is not None:
            prev_item.next = next_item

        if next_item is not None:
            next_item.prev = prev_item

        if self.head == item:
            # item was the first element in the list
            self.head = next_item

        if self.tail == item:
            # item was the last element in the list
            self.tail = prev_item

        # make sure that the item itself doesn't have references to other nodes
        item.prev = None
        item.next = None


class LRUCache(object):

    def __init__(self, maxSize, df_intial_objects):
        self.maxSize = maxSize
        self.cache_map = {}
        self.history = LinkedList()
        self.currentsize = 0.97 * maxSize
        self.initial_cache = df_intial_objects

    def set(self, key: int, value: int) -> str:
        """
        Add a new key-value pair to the cache.
        If key exists, replace its value by a new one.
        If maxSize is reached, evict the LRU item and insert a new pair
        """
       # evicted_key_str = '0'
        status = 'hit'

        value_node: Node = Node(key, value)

        if key not in self.cache_map:
            if key not in self.initial_cache:
                while (self.currentsize + value) >= self.maxSize:
                    # no space left, needs to evict the least recently used item
                    evicted_key = self.evict_least_recent_item()
#                    evicted_key_str = evicted_key_str + '_' + str(evicted_key)
                status = 'miss'
                self.currentsize += value
            self.cache_map[key] = value_node
            self.history.add_to_head(value_node)

        return status

    def evict_least_recent_item(self) -> int:
        """
        Evict the least recently used item
        """
        lru_item: Node = self.history.tail

        # we can add thr value of cache utilization condition here to evict entries which are below thr value

        if lru_item is None:
            return -1

        return_value = lru_item.key
        self.currentsize -= lru_item.value
        self.remove_item(lru_item)

        return return_value

    def remove_item(self, item: Node) -> None:
        """
        Remove item represented by node from the map and the list
        """
        self.history.unlink(item)
        del self.cache_map[item.key]