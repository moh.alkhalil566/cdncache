from subprocess import check_output
import os
import math

global firstRun
global parametersDict
    
def runCache(hostName, size, algo, parameter, parameterValue):
    pwd = '/Documents/AML/DSL2/repo/Results/trFeedingAlgorithms'
    command = ['docker', 'run', '-it', '-v', pwd + ':/trace', 'sunnyszy/webcachesim',
               "M2host" + str(hostName)+".tr", algo, str(size), '--objective=object_miss_ratio']
    if parameter:
        command = command + ["--" + str(parameter) + "="+str(parameterValue)]
    print(command)
    p = check_output(command)
    return p

def runAlgo(hostName, size, outf, algo, parameter='', parameterValue=[]):
    global firstRun
    if not firstRun:
       outf.write(",\n")
    firstRun = False
    os.system("docker rm $(docker ps -a -q  --filter ancestor=sunnyszy/webcachesim)")
    p = runCache(hostName, size, algo, parameter, parameterValue)
    st = p.decode("utf-8")
    start = st.find("trace_file")
    end = st.find("segment_rss")
    outf.write(st[start-3:end-3])
    outf.write("}")
    return outf

def runAlgos(hostName, size, algos, outf):
    for algo in algos:
        if algo in parametersDict:
            value = parametersDict[algo]
            for nested_key, nested_value in value.items():
                for item in nested_value:
                    outf = runAlgo(hostName, size, outf,
                                   algo, nested_key, item)
        else:
            outf = runAlgo(hostName, size, outf, algo)

def runAll(path, hostNames, algos, sizes):
    global firstRun
    outf = open(path + ".json", "w+")
    outf.write("[\n")
    firstRun = True
    for hostName in hostNames:
        for size in sizes:
            runAlgos(hostName, size, algos, outf)
        print(hostName , " Done")
    outf.write("\n]")
    outf.close()
    
def getSizeRange(start, stop, num_samples):
    log_range = []
    for i in range(num_samples):
        value = start * math.pow(stop/start, i/(num_samples-1))
        log_range.append(int(value))
    return log_range



### Experiment 1: Run all
""" parametersDict = {}
parametersDict["LRB"] = {'memory_window': [6710886, 67108864, 671088640]}
parametersDict["LRU"] = {'bloom_filter': [0, 1]}
parametersDict["ThLRU"] = {'t': [1073, 107374, 10737418]}
parametersDict["LeCaR"] = {'learning_rate': [0.1, 0.45, 0.7]}

hostNames = [7, 2, 4, 12]
algos = ["Random", "Inf", "Belady", "AdaptSize", "LRU", "ThLRU", "LeCaR", "LRB"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/01"
runAll(path, hostNames, algos, sizes) """

### Experiment 2: Tune memory_window in LRB
""" parametersDict = {}
parametersDict["LRB"] = {'memory_window': [6710, 67108, 671088, 6710886]}

hostNames = [7, 2, 4, 12]
algos = ["LRB"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/02"
runAll(path, hostNames, algos, sizes) """

### Experiment 3: Tune t in TLRU
""" parametersDict = {}
parametersDict["ThLRU"] = {'t': [10737, 1073741]}

hostNames = [7, 2, 4, 12]
algos = ["ThLRU"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/03"
runAll(path, hostNames, algos, sizes) """

### Experiment 4: Tune learning rate in LeCaR
""" parametersDict = {}
parametersDict["LeCaR"] = {'learning_rate': [0.9]}

hostNames = [7, 2, 4, 12]
algos = ["LeCaR"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/04"
runAll(path, hostNames, algos, sizes) """

### Experiment 5: Results per live, vod, and all
""" parametersDict = {}
parametersDict["LRB"] = {'memory_window': [6710, 67108, 671088]}
parametersDict["LRU"] = {'bloom_filter': [0, 1]}
parametersDict["ThLRU"] = {'t': [1073, 10737, 107374]}
parametersDict["LeCaR"] = {'learning_rate': [0.1, 0.45, 0.9]}

hostNames = ['live', 'vod', 'all']
algos = ["Random", "Inf", "Belady", "AdaptSize", "LRU", "ThLRU", "LeCaR", "LRB"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/05"
runAll(path, hostNames, algos, sizes) """

### Experiment 6: AdpatSize parameter
parametersDict = {}
parametersDict["AdaptSize"] = {'t': [50000, 200000]}

hostNames = [7]
algos = ["AdaptSize"]
sizes = getSizeRange(8e7, 2.5e9, 10)
path = "../Results/AlgorithmResults/New/06"
runAll(path, hostNames, algos, sizes)