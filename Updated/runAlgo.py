from subprocess import PIPE, run, check_output
import os

def runAlgos(name, size, method):
    print(name, size, method)
    pwd= os.getcwd()
    algos= ["Random","Inf","Belady","RelaxedBelady","AdaptSize","LRU","ThLRU","LeCaR","UCB","LRB"]
    outf = open("../../DSL2/Results/"+ str(method) + str(name)+ "-" + str(size)+ ".json", "w+")
    outf.write("[")
    for algo in algos:
        os.system("docker rm $(docker ps -a -q  --filter ancestor=sunnyszy/webcachesim)")
        p = check_output(['docker', 'run', '-it', '-v', pwd+':/trace', 'sunnyszy/webcachesim', str(method) + str(name)+".tr", algo, str(size)])
        st=p.decode("utf-8")
        start= st.find("trace_file")
        end= st.find("segment_rss")
        outf.write(st[start-3:end-3])
        outf.write("}")
        if algo != algos[-1]:
            outf.write(",")
        print(algo+ " Done")
        
    outf.write("]") 


names= [1, 7,2,4,12]
sizes= range(int(1e8),int(2e9),int(3e8))
method= "M1host"

for name in names:
    for size in sizes:
        runAlgos(name, size, method)
    
# docker run -it -v ${PWD}:/trace sunnyszy/webcachesim host2.tr LRU 100000000 > LRU-v.txt