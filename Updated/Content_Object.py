class Content_Object(object):
    
    def __init__(self, df_initial, object_id_set):
        self.objectid = df_initial.set_index(object_id_set).to_dict()['object_id']
        self.lastid = len(df_initial)
            
    def getid(self, key):
        if key not in self.objectid:
            self.objectid[key] = self.lastid
            self.lastid += 1
        return self.objectid[key]