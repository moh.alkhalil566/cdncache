from heapq import heappush, heappop

class OPT:
    def __repr__(self):
        return "OPT"

    def __init__(self, maxSize, df_intial_objects):
        self.maxSize = maxSize        # Cache size
        self.currentsize = .97 * maxSize       # Items in cache now (size)
        self.stored = {}  # Stored keys
        self.heap = []    # (-dist, key, size, valid)
        self.hitcount = 0
        self.count = 0
        self.nextref = {}
        self.initial_cache = df_intial_objects

    def getnextref(self, key):
        ne = float("inf")
        if key in self.nextref and self.nextref[key]:
            ne = self.nextref[key][-1]
        return ne

    def deletefurthest(self):
        valid = False
        delkey = None
        while not valid:
            try:
                deldist, delkey, s, valid = heappop(self.heap)
            except Exception as e:
                print(e, self.heap)
                raise Exception()

        realdeldist = -deldist
        item = self.stored[delkey]
        self.currentsize -= item[2]
        del self.stored[delkey]
        return delkey

    def setup(self, batch):
        batch['id'] = range(1, len(batch) + 1)
        self.nextref = batch.sort_values(['id'], ascending=False) \
            .groupby('object_id')['id'].apply(list).to_dict()
        for row in self.initial_cache.itertuples():
            key = row.object_id
            value = row.byte_sent
            dist = self.getnextref(key)
            item  = [-dist, key, value, True]
            self.stored[key] = item
            heappush(self.heap, item)


    def get(self, key, val=1):
        assert self.nextref[key], f'{key} not found'
        self.count += 1
        self.nextref[key].pop()
        if key in self.stored:
            old = self.stored[key]
            old[3] = False
            nr = self.getnextref(key)
            item = [-nr, key, old[2], True]
            self.stored[key] = item
            heappush(self.heap, item)
            self.hitcount += 1
            return 1
        return 0

    def put(self, key, s, val=1):
        if key not in self.stored:
            while self.currentsize + s > self.maxSize:
                delkey = self.deletefurthest()
            self.currentsize += s
            dist = self.getnextref(key)
            item  = [-dist, key, s, True]
            self.stored[key] = item
            heappush(self.heap, item)
    
    def apply(self, key, s):
        status = 'hit'
        ret = self.get(key)
        if not ret:
            status = 'miss'
            self.put(key, s, 1)
        return status